import { createSignal, createEffect, onCleanup } from "solid-js";
import {useStateStore} from "../utils/services/stateStore/store";

export const useContextMenu  = () => {
  const [clicked, setClicked] = createSignal(false);
  const {stateService:{ state, updateState } } = useStateStore();
  createEffect(() => {
    const handleClick = () => {
      setClicked(false);
      updateState('contextMenu', {...state.contextMenu, isOpened: false, clientX: 0, clientY: 0});
    };
    window.addEventListener("click", handleClick);
    //window.addEventListener("contextmenu", handleContextClick);
    onCleanup(() => {
      window.removeEventListener("click", handleClick);
     // window.removeEventListener("contextmenu", handleContextClick);
    });
  });

  return {
    clicked,
    setClicked
  }
}

export default useContextMenu;