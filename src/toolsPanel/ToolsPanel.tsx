import {createEffect, createSignal} from "solid-js";
import {Button} from "../common/components/button/Button";
import {Modal} from "../common/components/modal/Modal";

const ToolsPanel = () => {
  const [isOpenCreateProject, setIsOpenCreateProject] = createSignal(false);

  createEffect(() => {
    //console.log("isOpenCreateProject =", isOpenCreateProject());
  });

  return (
    <>
      <Button text='Создать проект' handleClick={() => setIsOpenCreateProject(true)}/>
      <Button text='Загрузить проект'/>
      <Button text='Создать скважину'/>
      {isOpenCreateProject() && <Modal title={"Создать проект"} handleClose={() => setIsOpenCreateProject(false)}>Modal window</Modal>
      }
    </>
  );
}

export default ToolsPanel;