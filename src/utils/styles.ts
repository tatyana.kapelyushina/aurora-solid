function mq(from: number, until?: number): string {
  const minWidth = from + 'px';
  if (until) {
    const maxWidth = (until - 1) + 'px';
    return `@media screen and (min-width: ${minWidth}) and (max-width : ${maxWidth})`;
  } else {
    return `@media screen and (min-width: ${minWidth})`;
  }
}

export {mq};