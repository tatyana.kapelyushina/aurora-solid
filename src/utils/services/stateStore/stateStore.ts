import { onMount } from "solid-js";
import { createStore } from "solid-js/store"

const initialValue = {
  contextMenuIsOpened: false,
  contextMenu: {
    isOpened: false,
    clientX: 0,
    clientY: 0
  }
}

const stateStore = createStore(initialValue);

export const StateService = () => {
  const [state, setState] = stateStore;

  onMount(() => {
    let keys = Object.keys(localStorage);
    for (let key of keys) {
      if (!key) return
      const keyString = localStorage.getItem(key);
      setState({...state, [key]: keyString});
    }
  })

  const updateState = (key, value) => {
    setState({...state, [key]: value});
  }


  return { state, updateState }
}