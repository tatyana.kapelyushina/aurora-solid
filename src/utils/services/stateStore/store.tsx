import { createContext, ParentComponent, useContext } from "solid-js"
import {StateService} from "./stateStore";

export type RootState = {
  stateService: ReturnType<typeof StateService>
}

const rootState: RootState = {
  stateService: StateService()
}

const StoreContext = createContext<RootState>();

export const useStateStore = () => useContext(StoreContext)!

export const StoreProvider: ParentComponent = (props) => {
  return <StoreContext.Provider value={rootState}>{props.children}</StoreContext.Provider>
}
