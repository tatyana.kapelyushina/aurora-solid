import { createGlobalStyles } from "solid-styled-components";
import {COLORS} from "../constants/styles";
import FiraSansBook from "../media/fonts/Firasansbook.woff";
import FiraSansBook2 from "../media/fonts/Firasansbook.woff2";
import AuroraEOT from "../media/fonts/aurora.eot";
import AuroraTTF from "../media/fonts/aurora.ttf";
import AuroraWOFF from "../media/fonts/aurora.woff";
import AuroraSVG from "../media/fonts/aurora.svg";


const GlobalStyle = createGlobalStyles`
    @font-face {
      font-family: 'Fira Sans';
      src: url(${FiraSansBook2}) format('woff2'), 
      url(${FiraSansBook}) format('woff');
      font-weight: 300;
      font-style: normal;
    }
    
    @font-face {
      font-family: 'aurora';
      src:  url(${AuroraEOT}?3qrat6);
      src:  url(${AuroraEOT}?3qrat6#iefix) format('embedded-opentype'),
        url(${AuroraTTF}?3qrat6) format('truetype'),
        url(${AuroraWOFF}?3qrat6}) format('woff'),
        url(${AuroraSVG}?3qrat6#aurora) format('svg');
      font-weight: normal;
      font-style: normal;
      font-display: block;
    }
    
    [class^="icon-"], [class*=" icon-"] {
      /* use !important to prevent issues with browser extensions that change fonts */
      font-family: 'aurora' !important;
      speak: never;
      font-style: normal;
      font-weight: normal;
      font-variant: normal;
      text-transform: none;
      line-height: 1;
    
      /* Better Font Rendering =========== */
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }
    
    .icon-arrow_left:before {
      content: "\\e901";
    }
    .icon-chevron_left:before {
      content: "\\e902";
    }
    .icon-clear:before {
      content: "\\e900";
    }

   * {
      box-sizing: border-box;
      font-family: 'Fira Sans';
      color: ${COLORS.gray6};
    }
   h1 {
      margin: 0;
      padding: 0;
      font-weight: 200;
      font-size: 38px;
    }
   h2 {
      margin: 0;
      padding: 0;
      font-weight: 300;
      font-size: 38px;
    }
   h3 {
      margin: 0;
      padding: 0;
      font-weight: 700;
      font-size: 38px;
    }
`;
export default GlobalStyle;