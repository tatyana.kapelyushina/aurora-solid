import {rgba} from "polished";
import { styled } from "solid-styled-components";
import {BREAKPOINTS, COLORS} from "../../../constants/styles";
import {mq} from "../../../utils/styles";

export const MODAL_HEADER_HEIGHT = 30;
export const MODAL_FOOTER_HEIGHT = 80;

export const ModalStyled = styled("section")`
  position: fixed;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  z-index: 100;
  color: ${COLORS.gray6};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px;
 
`;

export const ModalBackground = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: ${rgba(COLORS.gray7, 0.5)};
  z-index: 10;
`;

export const ModalContainer = styled("div")`
  display: flex;
  flex-direction: column;
  z-index: 20;
  background-color: ${COLORS.white};
  width: 100%;
  max-width: 600px;
`;

export const ModalHeader = styled("header")`
  display: flex;
  position: relative;
  width: 100%;
  height: ${MODAL_HEADER_HEIGHT}px;
  align-items: center;
  font-size: 20px;
  background-color: ${COLORS.gray10};
  color: ${COLORS.gray6};
  border: 1px solid ${COLORS.white};
`;

export const ModalHeaderTitle = styled("div")`
  width: 100%;
  padding: 10px;
  color: ${COLORS.gray6};
  font-size: 13px;
  text-transform: uppercase;
`;

export const ButtonClose = styled("div")`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  width: 25px;
  height: 20px;
  font-size: 16px;
  text-align: center;
  justify-content: center;
 
  &:before {
    font-family: 'aurora';
    content: "\\e900";
    line-height: 20px;
    position: absolute;
    top: 0;
    right: 5px;
    color: ${COLORS.gray6};
  }
`;

export const ModalContent = styled("div")`
  width: 100%;
  min-height: '100px';
  font-size: 18px;
  padding: 20px 10px;
`;