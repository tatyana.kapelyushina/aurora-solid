import { ModalStyled, ModalContainer, ModalBackground, ModalContent, ModalHeader, ModalHeaderTitle, ButtonClose } from "./Modal.styled";
import {Component, createEffect, createSignal, JSX} from "solid-js";

interface Props {
  children: JSX.Element;
  title: string;
  handleClose: (flag: boolean) => void;
}

export const Modal: Component<Props> = (props: Props) => {
  const {children, title, handleClose} = props;

  return (
    <>
      <ModalStyled role={"dialog"}>
        <ModalBackground onClick={handleClose}/>
        <ModalContainer>
          <ModalHeader>
            <ModalHeaderTitle>{title}</ModalHeaderTitle>
            <ButtonClose onClick={handleClose}></ButtonClose>
          </ModalHeader>
          <ModalContent>{children}</ModalContent>
        </ModalContainer>
      </ModalStyled>
    </>
  )
}