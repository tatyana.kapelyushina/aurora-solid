import { styled } from "solid-styled-components";
import {COLORS} from "../../../constants/styles";

export const Btn = styled("button")`
  background-color: white;
  border: 1px solid ${COLORS.gray2};
  margin-right: 5px;
`;