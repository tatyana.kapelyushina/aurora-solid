import {DOMElement} from "solid-js/jsx-runtime";
import { Btn } from "./Button.styled";

interface Props {
  text: string;
  handleClick: () => void;
}
export const Button = (props: Props) => {
  const { text, handleClick } = props;

  return (
    <Btn onClick={(e: MouseEvent & { currentTarget: HTMLButtonElement; target: DOMElement }) => {
      e.stopPropagation();
      handleClick();
    }}>{text}</Btn>
  )
}