import {ContextMenuWrapper} from "../../contextMenu/ContextMenuWrapper";
import {TNode} from "../models/TNode";
import { createEffect, Show, batch, mergeProps } from "solid-js";
import { createStore } from "solid-js/store";
import {useToggle} from "solidjs-hooks";
import Tree from "../Tree";
import {NodeBlock, ExpandIcon} from "./TreeNode.styled";
import {NodeContainer} from "../Tree.styled";
import { ThemeProvider, DefaultTheme } from "solid-styled-components";

export const TreeNode = (props: TNode) => {
  const {label, contextMenu, id, children} = props;
  const [showChildren, setShowChildren] = useToggle();
  const [theme, setTheme] = createStore<DefaultTheme>({
    viewExpanded: false
  });

  const handleClick = (e) => {
    setShowChildren();
    setTheme({ viewExpanded: showChildren() });
  }

  return (
    <>
      <ThemeProvider theme={theme}>
        <NodeBlock onClick={handleClick}>
          <Show when={children?.length > 0}><ExpandIcon></ExpandIcon>
          </Show>
          <ContextMenuWrapper id={id} contextMenu={contextMenu}>{label}</ContextMenuWrapper>
        </NodeBlock>
        <Show when={showChildren()}>
          <NodeContainer>
            <Tree treeData={children} />
          </NodeContainer>
        </Show>
      </ThemeProvider>
    </>
  )
}