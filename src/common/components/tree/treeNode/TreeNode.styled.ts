import {COLORS} from "../../../../constants/styles";
import { styled, css } from "solid-styled-components";

const expanded = `
  transform: rotate(180deg);
  top: 0;
  right: 0;
`

export const NodeBlock = styled("div")`
  display: flex;
`;

export const ExpandIcon = styled("div")`
  position: relative;
  width: 15px;
  height: 15px;
  color: ${COLORS.gray5};
  
  &:before {
    font-family: 'aurora';
    content: "\\e901";
    line-height: 18px;
    width: 10px;
    position: absolute;
    top: 6px;
    right: 6px;
    font-size: 20px;
    transform: rotate(-90deg);    
    ${props => props.theme.viewExpanded ? expanded : null};    
  }
`;