import { styled, css } from "solid-styled-components";

export const NodeContainer = styled("div")`
  padding: 0;
  margin: 0 0 0 10px;
`;