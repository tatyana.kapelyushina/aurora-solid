import {createSignal, For} from "solid-js";
import {TreeNode} from "./treeNode/TreeNode";
import {TNodeItem} from "./models/TNode";
import {NodeContainer} from "./Tree.styled";

interface Props {
  treeData: [];
}

const Tree = (props: Props) => {
const [tData, setTData] = createSignal(props.treeData);

  return (
    <NodeContainer>
      <For each={tData()}>
        {(treeNode: TNodeItem, index) => <TreeNode {...treeNode}></TreeNode>}
      </For>
    </NodeContainer>
  );
};

export default Tree;