import {ContextMenuItem} from "../../contextMenu/models/ContextMenuItem";

export interface TNodeItem {
  key: string;
  label: string;
  contextMenu?: ContextMenuItem[];
}

export interface TNode extends TNodeItem {
  children?: TNodeItem[];
}
