import { styled } from "solid-styled-components";
import {COLORS} from "../../../../constants/styles";

export const MenuWrapper = styled("div")`
  position: fixed;
  top: ${props => props.theme.y}px;
  left: ${props => props.theme.x}px;
  width: 220px;
  background-color: ${COLORS.white};
  z-index: 1000;
  padding: 5px;
  border-top: 1px solid ${COLORS.gray2};
  box-shadow: 2px 2px 2px 0px rgba(60,75,74,0.31);
  -webkit-box-shadow: 2px 2px 2px 0px rgba(60,75,74,0.31);
  -moz-box-shadow: 2px 2px 2px 0px rgba(60,75,74,0.31);
`;

export const MenuOption = styled("div")`
  padding: 0 0 3px 0;
`;

//${props => props.theme.viewExpanded ? expanded : null};