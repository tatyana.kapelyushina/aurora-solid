import { For } from "solid-js";
import { createStore } from "solid-js/store";
import {ContextMenu, MenuItem} from "../models/ContextMenuItem";
import {MenuWrapper, MenuOption} from "./Menu.styled";
import { ThemeProvider, DefaultTheme } from "solid-styled-components";

interface Props {
  x: number;
  y: number;
  menu: ContextMenu[];
}

export const Menu = (props: Props) => {
  const { x, y, menu } = props;
  const [theme, setTheme] = createStore<DefaultTheme>({
    x: x,
    y: y
  });
  return (
    <ThemeProvider theme={theme}>
      <MenuWrapper>
        <For each={menu['items']}>
          {(menuItem: MenuItem, index) => <MenuOption>{menuItem.name}</MenuOption>}
        </For>
      </MenuWrapper>
    </ThemeProvider>
  )
}