import { Show, ParentProps } from "solid-js";
import useContextMenu from "../../../hooks/useContextMenu";
import {useStateStore} from "../../../utils/services/stateStore/store";
import {ContextBlock, ContextLabel} from "./ContextMenuWrapper.styled";
import {Menu} from "./Menu/Menu";
import {ContextMenu} from "./models/ContextMenuItem";

interface Props {
  id: string;
  contextMenu: ContextMenu[];
}

export const ContextMenuWrapper = (props: ParentProps<Props>) => {
  const {id, contextMenu, children} = props;
  const { clicked, setClicked } = useContextMenu();
  const {stateService:{ state, updateState } } = useStateStore();

  const handleLabelClick = (e) => {
    e.preventDefault();
    updateState('contextMenu', {...state.contextMenu, isOpened: false, clientX: 0, clientY: 0});
    if (!state.contextMenuIsOpened) {
      updateState('contextMenu', {...state.contextMenu, isOpened: true, clientX: e.clientX, clientY: e.clientY});
      setClicked(true);
    }
  };

  return (
    <>
      <ContextBlock>
        <ContextLabel id={id} oncontextmenu={handleLabelClick}>
          {children}
        </ContextLabel>
        <Show when={Boolean(contextMenu) && clicked() && state.contextMenu.isOpened}>
          <Menu x={state.contextMenu.clientX} y={state.contextMenu.clientY} menuId={contextMenu.id} menu={contextMenu}/>
        </Show>
      </ContextBlock>
    </>
  )
}