export interface ContextMenu {
  id: string;
  items: MenuItem[];
}

export interface MenuItem {
  id: string;
  name: string;
}