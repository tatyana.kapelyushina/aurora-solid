export const HEADER = 40;
export const FOOTER = 40;

const COLORS = {
  white: '#FFFFFF',
  gray1: '#eef2f0',
  gray2: '#d2dbd8',
  gray3: '#8c8c8c',
  gray4: '#595959',
  gray5: '#3c4b4a',
  gray6: '#191f1c',
  gray7: '#586f6e',
  gray8: '#6f8c8a',
  gray9: '#a8b9af',
  gray10: '#dde4e3'

}

const BREAKPOINTS = {
  mobile: 320,
  tablet: 768,
  tabletLarge: 1024,
  laptop: 1200,
  desktop: 1440,
  desktopLarge: 1680,
  desktopHuge: 1920
};

export {
  BREAKPOINTS, COLORS
};