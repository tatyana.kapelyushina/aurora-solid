export const TREEDATA = [
  {
    id: "0",
    key: "0",
    label: "Documents",
    contextMenu: {
      id: "0",
      items: [
      {
        id: '0-0',
        name: "Option 1"
      },
      {
        id: '0-1',
        name: "Option 2"
      }
    ]
},
    children: [
      {
        id: "0-0",
        key: "0-0",
        label: "Document 1-1",
        children: [
          {
            id: "0-1-1",
            key: "0-1-1",
            label: "Document-0-1.doc",
          },
          {
            id: "0-1-2",
            key: "0-1-2",
            label: "Document-0-2.doc",
          },
        ],
      },
    ],
  },
  {
    id: "1",
    key: "1",
    label: "Desktop",
    children: [
      {
        id: "1-0",
        key: "1-0",
        label: "document1.doc",
      },
      {
        id: "1-0",
        key: "0-0",
        label: "documennt-2.doc",
      },
    ],
  },
  {
    id: "2",
    key: "2",
    label: "Downloads",
    contextMenu: {
      id: "2",
      items: [
        {
          id: '2-0',
          name: "Downloads Option 1"
        },
        {
          id: '2-1',
          name: "Downloads Option 2"
        }
      ]
    },
    children: [],
  },
];