import GlobalStyle from './utils/globalStyle'
import Dashboard from "./dashboard/Dashboard";
import {StoreProvider} from "./utils/services/stateStore/store";

const App = () => {
  return (
    <>
      <StoreProvider>
        <Dashboard />
        <GlobalStyle />
      </StoreProvider>
    </>
  );
};

export default App;
