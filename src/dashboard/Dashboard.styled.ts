import {styled} from "solid-styled-components";
import {HEADER, FOOTER, COLORS} from "../constants/styles";

export const LEFT_COLUMN_WIDTH = '200px';

export const RootContainer = styled("div")`
  min-height: 100vh;
`;

export const Header = styled("header")`
  display: flex;
  height: ${HEADER}px;
  border-bottom: 1px solid ${COLORS.gray2};
`;

export const Logo = styled("div")`
  width: ${LEFT_COLUMN_WIDTH};
  border-right: 1px solid ${COLORS.gray2};
`;

export const HeaderSection = styled("div")`
  background-color: ${COLORS.gray1};
  display: flex;
  flex: 1;
  padding: 5px;
`;

export const ContentWrap = styled("div")`
  position: relative;
  display: flex;
  flex-direction: row;
  min-height: calc(100vh - ${HEADER + FOOTER}px);
`;

export const Aside = styled("aside")`
  width: ${LEFT_COLUMN_WIDTH};
  border-right: 1px solid ${COLORS.gray2};
`;

export const Section = styled("section")`
  flex: 1;
`;

export const Footer = styled("footer")`
  background-color: lightblue;
  color: white;
  height: ${FOOTER}px;
  border-top: 1px solid ${COLORS.gray2};
  background-color: ${COLORS.gray1};
`;