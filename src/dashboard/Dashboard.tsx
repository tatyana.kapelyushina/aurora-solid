import Tree from "../common/components/tree/Tree";
import {TREEDATA} from "../constants/treeData";
import ToolsPanel from "../toolsPanel/ToolsPanel";
import { RootContainer, Header, Logo, HeaderSection, ContentWrap, Aside, Section, Footer } from "./Dashboard.styled";

const Dashboard = () => {
  return (
    <RootContainer>
      <Header>
        <Logo></Logo>
        <HeaderSection>
          <ToolsPanel/>
        </HeaderSection>
      </Header>
      <ContentWrap>
        <Aside><Tree treeData={TREEDATA}/></Aside>
        <Section><h1>Example</h1></Section>
      </ContentWrap>
      <Footer>3</Footer>
    </RootContainer>
  );
};

export default Dashboard;